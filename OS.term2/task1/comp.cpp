#include <iostream>
#include <cstdio>
#include <algorithm>
#include <unistd.h>

using namespace std;

char token;

struct Base 
{
    bool is_function;
};

struct Expression : Base
{
    Expression* left;
    Expression* right;
    bool leaf;
    string operation;

    Expression(Expression* left, Expression* right, string operation)
    {
        this->left = left;
        this->right = right;
        this->operation = operation;
    }

};

struct Variable 
{
    string id;
    expression value;

};

struct Function : Base
{
    string id;
    vector<Expression*> arguments;
    vector<Function*> links;
    vector<Expression*> expressions;
    vector<bool> order;
};


map<String, Expression*> global_variables;
vector<Function*> functions;
Function* main_function;
bool function_scope = false;

string current_line;
string token;
int token_index = 0;

bool is_letter(string s)
{
    return s[0] >= 'a' && s[0] <= 'z';
}

void next_token()
{
    token = "";
    if (token_index < current_line.length())
    {
        token = current_line[token_index++];
        while (token_index < current_line.length() && is_letter(current_line[token_index]))
        {
            token += current_line[token_index++];
        }
    }
}

void remove_spaces()
{
    token_index = 0;
    string line = "";
    for (int i = 0; i < current_line.length(); i++)
        if (current_line != ' ')
            line += current_line[i];
    current_line = line;
}

void assert(string s, string p)
{
    if (s != p)
    {
        printf("Error in code");
        exit(0);
    }
}

Base* parse_function()
{
    remove_spaces();
    Base* function = new Function();
    next_token();
    function->id = token;
    next_token();
    assert(token, "(");
    next_token();
    while (token != ")")
    {
        if (token == ',')
            continue;

        Expresion* arg = new Expression();
        arg->operation = token;
        arg->left = true;
        function->arguments.push_back(arg);

        next_token();
    }
    next_token();
    assert(token, "}");
    return function;
}

Base* parse_arithmetic_expression()
{


}

Base* parse_expression()
{
    remove_spaces();
    next_token();
    next_token();
    token_index = 0;
    if (token == '(')
    {
        return parse_function();
    }
    else
    {
        return parse_arithmetic_expression();
    }
}

void parse_line()
{
    next_token();
    if (token == "def")
    {
        current_line.erase(0, 4);
        function_scope = true;
        Function* function = parse_function();
        functions.push_back(function);
    }
    else
    {
        Base expression = parse_expression();
        if (function_scope)
        {
            Function* last_function = functions[functions.size() - 1];
            if (expression.is_function)
            {
                last_function->links.push_back(&((Function)expression));
                last_function->order.push_back(true);
            }
            else
            {
                last_function->expressions.push_back(&((Expression)expression));
                last_function->order.push_back(false);
            }  
        }
        else
        {
            Expression global = (Expression)expression;
            global_variables[global.id] = &global;
        }

    }
}

void format_line()
{
    int i = 0;
    while (i < current_line.length() && current_line[i] == ' ')
        i++;
    current_line.erase(0, i);
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        fprintf(stdout, "Error in arguments");
        return 1;
    }
    
    freopen(argv[1], "r", stdin);
    while (readline(cin, current_line))
    {
        format_line();
        if (current_line == "")
            continue;
        parse_line();
    }

    return 0;
}

