#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_SIZE 512

int uid;
mode_t modes;

mode_t get_modes(char* str_modes, int l, int r)
{
    l++, r--;
    int length = (r - l + 1);
    if (length > 2) {
        exit(EXIT_FAILURE);
    }

    if (length == 2)
        return O_RDWR;
    if (str_modes[l] == 'r')
        return O_RDONLY;
    else
        return O_WRONLY;
}

int my_atoi(char* buffer, int l, int r)
{
    int result = 0;
    for (int i = l; i < r; i++)
    {
        result = result * 10 + buffer[i] - '0'; 
    }

    return result;
}

void parse_arguments(char* buffer, int l, int r, int cnt)
{
    if (cnt == 0)
        uid = my_atoi(buffer, l, r);
    if (cnt == 1)
    {
        get_modes(buffer, l, r);
    }

}

void write_all(int out, char* buffer, int count)
{
    int pos = 0;
    while (pos < count)
    {
        int write_count =  write(out, buffer + pos, count - pos);
        if (write_count == -1)
            exit(EXIT_FAILURE);
        pos += write_count;
    }
}

void write_yes()
{
    write_all(1, "yes", 3);
}

void write_no()
{
    write_all(1, "no", 2);
}

int main(int argc, char* argv[])
{
//    printf("%d\n", getuid());
    char* buffer = (char*)malloc(BUFFER_SIZE * sizeof(char*));
    int length = 0;
    while (1)
    {
        int count = read(0, buffer + length, BUFFER_SIZE - length);
        int last_delimiter;
        for (int j = count - 1; j >= 0; j--)
            if (buffer[j] == '\0')
            {
                last_delimiter = j;
                break;
            }
        if (count < 0)
            exit(EXIT_FAILURE);
        if (!count)
            break;
        int last = 0; 
        int cnt = -1;
        for (int i = 0; i <= last_delimiter; i++)
            if (buffer[i] == ' ' || buffer[i] == '\0')
            {
                if (buffer[i] == '\0')
                {
                    int result = seteuid(uid);
                    if (result < 0)
                    {
                        write_no();
                        return 1;
                    }
                    int fd = open(buffer + last + 1, modes);

                    if (fd < 0)
                    {
                        write_no();
                    }
                    setuid(0);
                    cnt = -1;
                    last = i + 1;
                    write_yes();
                    close(fd);
                    continue;
                }
                cnt++;
                cnt %= 2;
                parse_arguments(buffer, last, i, cnt); 
                last = i;
            }
            length = count - last_delimiter;
            memmove(buffer, buffer + last_delimiter, count - last_delimiter);
        }
    write_yes();
    return 0;
}
