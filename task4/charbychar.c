#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define BUFFER_SIZE 1024

void write_all(int out, char* buffer, int count)
{
    int pos = 0;
    while (pos < count)
    {
        int write_count = write(out, buffer + pos, count - pos);
        if (write_count == -1)
        {
            exit(EXIT_FAILURE);
        }
        pos += write_count;
    }
}

int main(int argc, char **argv)
{
    for (int i = 1; i < argc - 1; i += 2)
    {
        int count = atoi(argv[i]);
        int seconds = atoi(argv[i + 1]);
        
        char* buffer = (char*)malloc(BUFFER_SIZE); 
        int pos = 0;
        while (1)
        {
            int read_count = read(0, buffer + pos, count - pos);
            if (read_count < 0)
            {
                exit(EXIT_FAILURE);
            }
            else if (read_count == 0)
            {
                break;
            }
            pos += read_count;
        }

        write_all(1, buffer, count);
        sleep(seconds);
    }
    
    while (1)
    {
        int count = 1;
        int seconds = 0;
        char* buffer = (char*)malloc(BUFFER_SIZE);
        int pos = 0;
        while (1)
        {
            int read_count = read(0, buffer + pos, count - pos);
            if (read_count < 0)
            {
                exit(EXIT_FAILURE);
            }
            else if (read_count == 0)
            {
                break;
            }
            pos += read_count;
        }
        
        write_all(1, buffer, count);
        sleep(seconds);
    }

}

