#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <memory.h>

#define BUFFER_SIZE 1024

int main(int argc, char **argv)
{
    if (argc % 2 == 0)
    {
        return 1;
    }

    struct pollfd fds[argc - 1];

    for (int i = 0; i < argc - 1; i += 2) 
    {
        int q = open(argv[i + 1], O_RDONLY);
        fds[i].fd = atoi(argv[i + 1]);
        fds[i].events = POLLIN;

        q = open(argv[i + 2], O_WRONLY);
        fds[i + 1].fd = atoi(argv[i + 2]);
        fds[i + 1].events = POLLOUT;
    }
    
    char ** buffer = (char**)malloc(argc - 1); 
    for (int i = 0; i < argc - 1; i++)
    {
        buffer[i] = (char*)malloc(BUFFER_SIZE * sizeof(char*));
    }

    int* is_eof = (int*)malloc((argc - 1) * sizeof(int*));
    int* size = (int*)malloc((argc - 1) * sizeof(int*)); 

    while (1)
    {
        int result = poll(fds, argc - 1, -1);
        if (errno == EINTR)
            continue;
        if (result == -1)
        {
            perror("Poll don't work");
            return 1;
        }
        for (int i = 0; i < argc - 1; i += 2)
        {
            if (fds[i].revents & (POLLIN | POLLHUP))
            {
                int read_count = read(fds[i].fd, buffer[i / 2] + size[i], BUFFER_SIZE - size[i]); 
                if (read_count <= 0)
                {
                    fds[i].fd = -1;
                }
                else
                {
                    if (fds[i + 1].fd < 0)
                        fds[i + 1].fd = -fds[i + 1].fd - 1;

                    size[i] += read_count;

                    if (size[i] == BUFFER_SIZE) 
                    {
                        if (!is_eof[i] && fds[i].fd > 0)
                            fds[i].fd = -fds[i].fd - 1;
                    }

                }
            }
            
            if (fds[i + 1].revents & POLLOUT)
            {
                int write_count = write(fds[i + 1].fd, buffer[i / 2], size[i]);
                if (write_count < 0)
                {
                    fds[i].fd = -1;
                    fds[i + 1].fd = -1;
                } 
                else
                {
                    size[i] -= write_count;
                    memcpy(buffer[i / 2], buffer[i / 2] + write_count, BUFFER_SIZE - write_count);

                    if (fds[i].fd < 0 && fds[i].fd != -1)
                    {
                        fds[i].fd = -fds[i].fd - 1;
                    }
                
                    if (size[i] == 0)
                    {
                        if (fds[i + 1].fd > 0)
                            fds[i + 1].fd = -fds[i + 1].fd - 1;
                    }
                }
            } 

            int count = 0;
            for (int j = 0; j < argc - 1; j++)
                if (fds[j].fd < 0)
                    count++;

            if (count == argc - 1)
                return 0;
        }
    }
}

