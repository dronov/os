#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

const int BUFFER_SIZE = 512;
const int OUT = 1;

void write_all(int out, char* buffer, int count)
{
    int pos = 0;
    while (pos < count)
    {
        int write_count =  write(out, buffer + pos, count - pos);
        if (write_count == -1)
            exit(EXIT_FAILURE);
        pos += write_count;
    }
}


int main(int argc, char* argv[])
{
    char* buffer = (char*)malloc(BUFFER_SIZE);
    char* delimiter = argv[0];
    for (int i = 2; i < argc; i++)
    {
        int fd = atoi(argv[i]);
        while (1)
        {
            int read_count = read(fd, buffer, BUFFER_SIZE);
            if (read_count < 0)          // IO problems
                exit(EXIT_FAILURE); 
            if (!read_count)            // EOF
                break;
            write_all(OUT, buffer, read_count);
        }
        if (i < argc - 1)
            write_all(OUT, delimiter, strlen(delimiter));
        close(fd);
    }
}
