#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>


int MAX_INT_SIZE = 32;

char* convert(int fd)
{
    char* buffer = (char*)malloc(MAX_INT_SIZE);
    sprintf(buffer, "%d", fd);
    return buffer;
}

int main(int argc, char* argv[])
{
    if (argc < 2)
        exit(EXIT_FAILURE);
    char ** child_args = (char**)malloc((argc + 1) * sizeof(char*));
    int* fd_array = (int*)malloc(argc * sizeof(int*));
    if (child_args == NULL || fd_array == NULL)
       exit(EXIT_FAILURE);
    child_args[0] = argv[1];    // contains delimiter
    child_args[1] = argv[2];    // contains "fdscat" 
    int count = 0;
    for (int i = 3; i < argc; i++)
    {
        int fd = open(argv[i], O_RDONLY);
        if (fd == -1)
        {
            free(child_args);
            exit(EXIT_FAILURE);
        }
        child_args[i - 1] = convert(fd); 
        fd_array[count++] = fd;
    }
    child_args[argc - 1] = NULL ; // NULL at the end, need to FORK()
    
    pid_t pid, child_pid;
    int status;
    switch (child_pid = fork())
    {
        case (pid_t)-1 :  // bad fork
            perror("Fork is fail");
            break;
        case (pid_t)0 :   //we are inside child process
            execvp(argv[2], child_args);
            break;
        default:         //we are inside parent process
            pid = wait(&status); 
    }
    //if child process was stopped or killed
    if (pid < 0)
        exit(EXIT_FAILURE);
    // close all file descriptors
    for (int i = 0 ; i < count; i++) 
    {
        close(fd_array[i]);
    }

    free(child_args);
    free(fd_array);
    exit(0);
}
