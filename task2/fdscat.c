#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>

const int BUFFER_SIZE = 512;
const int OUT = 1;

void write_all(int out, char* buffer, int count)
{
    int pos = 0;
    while (pos < count)
    {
        int write_count =  write(out, buffer + pos, count - pos);
        if (write_count == -1)
            exit(EXIT_FAILURE);
        pos += write_count;
    }
}

void write_reverse_string(int out, char* buffer, int l, int r)
{
    char* reverse_buffer = (char*)malloc(BUFFER_SIZE);
    int count = 0;
    for (int i = r - 1; i >= l; i--)
        reverse_buffer[count++] = buffer[i];
    reverse_buffer[count++] = buffer[r];

    write_all(out, reverse_buffer, count); 
    free(reverse_buffer);
}


int main(int argc, char* argv[])
{
    char* buffer = (char*)malloc(BUFFER_SIZE);
    int length = 0;
    int count = 0;
    int buffer_size;
    for (int i = 1; i < argc; i++)
    {
        int fd = atoi(argv[i]);
        char* buffer2 = (char*)malloc(BUFFER_SIZE);
        while (1)
        {
            int read_count = read(fd, buffer + length, BUFFER_SIZE - length);
            if (read_count < 0)          // IO problems
                exit(EXIT_FAILURE); 
            length += read_count;
            if (!read_count && buffer_size != 0) 
            {
                write_reverse_string(OUT, buffer2, 0, buffer_size);
                buffer_size = 0;
            } 
            else if (!read_count)            // EOF
                break;
            int last_delimeter = 0; 
            for (int j = 0; j < length; j++)
            {
                if (buffer[j] == '\n')
                {
                    count++;
                    if (count == 1)
                    {
                        memcpy(buffer2, buffer + last_delimeter, j - last_delimeter + 1);
                        buffer_size = j - last_delimeter;
                    }
                    else if (count == 2)
                    {
                        count = 0;
                        write_reverse_string(OUT, buffer, last_delimeter, j);
                        write_reverse_string(OUT, buffer2, 0, buffer_size);
                        buffer_size = 0;
                    }

                    last_delimeter = j + 1;
                }
            }
            length -= last_delimeter; 
            if (length) 
            {
                memmove(buffer, buffer + last_delimeter, length);
            }
        }
        free(buffer2);
        close(fd);
    }
    free(buffer);
}
