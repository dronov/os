#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <memory.h>

#define BUFFER_SIZE 1024

#define FDIN 0
#define FDOUT 1

int checked_call(int result)
{
    if (result < 0)
        exit(EXIT_FAILURE);
    return result;
}

void* _malloc(int size)
{
    void* result = malloc(size);
    if (result == NULL)
        exit(EXIT_FAILURE);
    return result;
}

void exec_command(char** command, int fd_in, int fd_out)
{
    int pid;
    pid = checked_call(fork());
    if (pid)
    {
        if (fd_in != FDIN)
            checked_call(close(fd_in));
        if (fd_out != FDOUT)
            checked_call(close(fd_out));
    } else
    {
        checked_call(dup2(fd_in, FDIN));
        checked_call(dup2(fd_out, FDOUT));
        execvp(*command, command);
        exit(1);
    }
}

char* parse(char* buffer, char **argv)
{
    while (*buffer != '\n' && *buffer != '\0')
    {
        while (*buffer == ' ' || *buffer == '\n')
            *buffer++ = '\0';
        *argv++ = buffer;
        while (*buffer != '\0' && *buffer != '\n' && *buffer != ' ')
            buffer++;
    }
    *buffer = '\0';
    *argv = '\0'; 
}

char** parse_command(char delimiter, char* buffer, int last, int current)
{
    int count_arguments = 0;
    for (int i = last; i <= current; i++)
        if (buffer[i] == delimiter || buffer[i] == '\n' || buffer[i] == '\0')
        {
            count_arguments++;
        }
    char** arguments = (char**)_malloc(count_arguments + 1);
    char* command = (char*)_malloc(current - last + 1);
    memcpy(command, buffer + last, current - last + 1);
    parse(command, arguments); 
    return arguments;
}

int input_fd, output_fd;
int main(int argc, char* argv[])
{
    input_fd = open(argv[1], O_RDONLY);
    output_fd = open(argv[2], O_WRONLY);

    char* buffer = (char*)_malloc(BUFFER_SIZE);
    int length = 0;
    while (1)
    {
        int read_count = checked_call(read(FDIN, buffer + length, BUFFER_SIZE - length));
     //   _print(buffer, "BUFFER IN MAIN");
        if (read_count == 0 && length == 0)
            break;
        length += read_count;
        int last_delimiter = 0;
        for (int i = 0; i < length; i++)
            if (buffer[i] == '\n')
            {
                char** command = parse_command(' ', buffer, last_delimiter, i);
                int fds[2];
                checked_call(pipe(fds));
                exec_command(command, input_fd, fds[1]);

                input_fd = fds[0];
                last_delimiter = i + 1;
            }
        length -= last_delimiter;
        memmove(buffer, buffer + last_delimiter, length);
    }
    
    char** cat_command = (char**)_malloc(2);
    cat_command[0] = "cat";
    cat_command[1] = NULL;
    exec_command(cat_command, input_fd, output_fd);
    return 0;
}
