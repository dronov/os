#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <memory.h>

#define BUFFER_SIZE 1045
#define FDIN 0
#define FDOUT 1

void* _malloc(int size)
{
    void* result = malloc(size);
    if (result == NULL)
        exit(EXIT_FAILURE);
    return result;
}

int checked_call(int result)
{
    if (result < 0)
        exit(EXIT_FAILURE);
    return result;
}

int last_pid;

void exec_command(char** command, int fd_in, int fd_out)
{
    int pid = checked_call(fork());
    if (pid)
    {
        last_pid = pid;
        if (fd_in != FDIN)
            checked_call(close(fd_in));
        if (fd_out != FDOUT)
            checked_call(close(fd_out));
    } else
    {
        checked_call(dup2(fd_in, FDIN));
        checked_call(dup2(fd_out, FDOUT));
        execvp(*command, command);
        exit(1);
    }
}

char* parse(char* buffer, char **argv)
{
    while (*buffer != '\n' && *buffer != '\0')
    {
        while (*buffer == ' ' || *buffer == '\n')
            *buffer++ = '\0';
        *argv++ = buffer;
        while (*buffer != '\0' && *buffer != '\n' && *buffer != ' ')
            buffer++;
    }
    *buffer = '\0';
    *argv = '\0'; 
}

void _print(char* buffer, char* message)
{
    printf("DEBUG-----in\n");
    printf("%s\n", message);
    for (int i = 0; buffer[i] != '\0' && buffer[i] != '\n'; i++)
        printf("%c", buffer[i]);
    printf("DEBUG-----out\n");
}


char** parse_command(char delimiter, char* buffer, int last, int current)
{
    int count_arguments = 0;
    for (int i = last; i <= current; i++)
        if (buffer[i] == delimiter || buffer[i] == '\n' || buffer[i] == '\0')
        {
            count_arguments++;
        }
    char** arguments = (char**)_malloc(count_arguments + 1);
    char* command = (char*)_malloc(current - last + 1);
    memcpy(command, buffer + last, current - last + 1);
    parse(command, arguments); 
    //printf("arguments in fork\n");
    //for (int i = 0; i < count_arguments; i++)
   // {
   //     printf("%s\n", arguments[i]);
   // }

    return arguments;
}

int stack[BUFFER_SIZE];
int stack_index = 0;
void pipe_runner(char* buffer, int length, int background)
{
    int input_fd = 0;
    int pid = checked_call(fork());
    if (pid == 0)
    {
        if (background)
            checked_call(setpgid(0, 0));
        int last_pipe = 0;
     //   _print(buffer, "buffer in child process");
        for (int i = 0; i < length; i++)
            if (buffer[i] == '|' || buffer[i] == '\n')
            {
                buffer[i] = '\n';
                char** command = parse_command(' ', buffer, last_pipe, i);
                int fds[2];
                checked_call(pipe(fds));
                exec_command(command, input_fd, fds[1]);
                input_fd = fds[0];
                last_pipe = i + 1;
            }
        char** cat_command = (char**)_malloc(2);
        cat_command[0] = "cat";
        cat_command[1] = NULL;
        exec_command(cat_command, input_fd, FDOUT);

        checked_call(waitpid(last_pid, NULL, 0));
        printf("bred\n");
        exit(0); 
    } else
    {
        if (background == 1)
        {
            setpgid(pid, pid);
        }

        if (background == 0)
            checked_call(waitpid(pid, NULL, 0));

        if (background)
            stack[stack_index++] = pid;
    }
}

void wait_child(char* buffer, int length)
{
    int pid = atoi(buffer);
    waitpid(stack[stack_index - pid - 1], NULL, 0);
}

void kill_child(char* buffer, int length)
{
    int pid = atoi(buffer);
    killpg(stack[stack_index - pid - 1], SIGTERM);
}

int main(int argc, char* argv[])
{
    char* buffer = (char*)_malloc(BUFFER_SIZE);
    int length = 0;
    while (1)
    {
        printf(" going to read\n");
        int read_count = checked_call(read(FDIN, buffer + length, BUFFER_SIZE - length));
        if (read_count == 0 && length == 0)
            break;
        length += read_count;
        int last_delimiter = 0;
        for (int i = 0; i < length; i++)
            if (buffer[i] == '\n')
            {
                char* query = _malloc(i - last_delimiter);
                memcpy(query, buffer + last_delimiter, i - last_delimiter + 1);
                switch (query[0])
                {
                    case 'r':
                        pipe_runner(query + 4, length, 0);
                        break;
                    case 'b':
                        pipe_runner(query + 4, length, 1);
                        break;
                    case 'w':
                        wait_child(query + 5, length);
                        break;
                    case 'k':
                        kill_child(query + 5, length);
                        break;
                }
                last_delimiter = i + 1;
            }
        length -= last_delimiter;
        memmove(buffer, buffer + last_delimiter, length);
    }
    return 0;
}

